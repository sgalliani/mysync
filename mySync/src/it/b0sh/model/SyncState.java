package it.b0sh.model;

/**
 * Created by Simone on 31/01/2016.
 */
public enum SyncState {

    NOT_STARTED,
    EXPLORING_SOURCE_PATH,
    COPING_FILE_AND_DIR,
    EXPLORING_DEST_PATH,
    REMOVING_FILE,
    FINISHED


}
