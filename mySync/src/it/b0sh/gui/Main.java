package it.b0sh.gui;

import it.b0sh.Syncronizer;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.apache.commons.cli.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Simone on 31/01/2016.
 */
public class Main extends Application {


    private static boolean fullSync;
    private static String source;
    private static String destination;
    private static Syncronizer batch;
    private static Thread workerThread;
    private static Thread guiThread = null;
    private static Properties prop;

    public static void main(String[] args) {

        parseArgs(args);

        workerThread = new Thread(batch = new Syncronizer(false,source,destination,fullSync));

        try {

            prop = new Properties();
            InputStream in = Main.class.getResourceAsStream("Main.properties");
            prop.load(in);
            in.close();


            launch(args);

        } catch (IOException e) {
            throw new RuntimeException("Properties file not found");
        }


    }

    @Override
    public void start(Stage primaryStage) throws Exception {



        primaryStage.setTitle("MySync");

        FXMLLoader loader =  new FXMLLoader(getClass().getResource("MainView.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root, 477, 217);
        primaryStage.setScene(scene);

        primaryStage.setOnHiding(new EventHandler<WindowEvent>() {

            public void handle(WindowEvent event) {

                if (batch.isFinished()) {
                    primaryStage.close();
                    Platform.exit();
                    System.exit(0);
                    return;
                }

                final Stage dialog = new Stage();
                Label label = new Label("Syncing in progress. Really exit?");
                Button okButton = new Button("OK");
                okButton.setOnAction(new EventHandler<ActionEvent>() {

                    public void handle(ActionEvent event) {
                        dialog.close();
                    }
                });
                Button cancelButton = new Button("Cancel");
                cancelButton.setOnAction(new EventHandler<ActionEvent>() {

                    public void handle(ActionEvent event) {
                        workerThread.interrupt();
                        guiThread.interrupt();
                        primaryStage.close();
                        dialog.close();
                        Platform.exit();

                    }
                });
                FlowPane pane = new FlowPane(10, 10);
                pane.setAlignment(Pos.CENTER);
                pane.getChildren().addAll(okButton, cancelButton);
                VBox vBox = new VBox(10);
                vBox.setAlignment(Pos.CENTER);
                vBox.getChildren().addAll(label, pane);
                Scene scene1 = new Scene(vBox);
                dialog.setScene(scene1);
                dialog.show();
            }
        });


        primaryStage.show();

        workerThread.start();

        MainViewController controller = (MainViewController)loader.getController();
        controller.progressBar.setProgress(0);


        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {

                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {

                        controller.qualeFaseLabel.setText( prop.getProperty( batch.getSyncState().name() ) );
                        controller.readErrorLabel.setText(Integer.toString(batch.getReadErrorList().size()));
                        controller.writeErrorLabel.setText(Integer.toString(batch.getWriteErrorList().size()));

                        if (controller.progressBar.getProgress() != (((double) batch.getCurrentStepProgress()) / 100)) {
                            controller.progressBar.setProgress(((double) batch.getCurrentStepProgress()) / 100);
                            System.out.println("Progress :" + controller.progressBar.getProgress());
                        }

                    }
                });

            }
        }, 0, 500);



    }

    private static void parseArgs(String[] args){
        Options options = new Options();
        Option sourcePath = new Option("s",true,"source path");
        Option destinationPath = new Option("d",true,"destination path");
        Option help = new Option("h",false,"this Help");
        Option fullSyncOption = new Option("f",false,"Full Sync mode");
        options.addOption(help);
        options.addOption(sourcePath);
        options.addOption(destinationPath);
        options.addOption(fullSyncOption);
        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(options,args);
            if(cmd.hasOption("h")){
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("java -jar mysync.jar",options);
                batch.setHasUnhandledErrors(false);
                System.exit(0);
            }

            if(cmd.hasOption("s")){
                source = cmd.getOptionValue("s");

            } else {
                System.exit(0);
            }

            if(cmd.hasOption("d")){
                destination = cmd.getOptionValue("d");

            } else {
                System.exit(0);
            }

            if(cmd.hasOption("f")){
                fullSync = true;
            }

        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }


}
