package it.b0sh;

import org.apache.commons.lang3.StringUtils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class Utils {

    public static NumberFormat getEuroFormat(){
        return NumberFormat.getCurrencyInstance(new Locale("it","IT"));
    }

    public static SimpleDateFormat getDateFormat(){
        return new SimpleDateFormat( "dd/MM/yyyy");
    }

    public static SimpleDateFormat getLongDateFormat(Locale locale){
        return new SimpleDateFormat( "dd MMMM yyyy" );
    }

    public static SimpleDateFormat getDateTimeFormat(){
        return new SimpleDateFormat( "dd/MM/yyyy HH:mm" );
    }

    public static SimpleDateFormat getTimeFormat(){
        return new SimpleDateFormat( "HH:mm" );
    }



    public static Long daysBetween( Date d1, Date d2 ){
        Long diff = d2.getTime() - d1.getTime();
        return diff / (24 * 60 * 60 * 1000);
    }

    public static Date addDays( Date date, int days ) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime( date );
        calendar.add( Calendar.DATE, days );
        return calendar.getTime();
    }

	/** A table of hex digits */
	private static final char[] hexDigit = { '0', '1', '2', '3', '4', '5', '6',
			'7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	public static boolean nullsafeEquals(Object a, Object b) {
		if (a == null ^ b == null)
			return false;
		return a == null || a.equals(b);
	}

	public static String exceptionToString(Throwable e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		return sw.toString();
	}

	/**
	 * Come Strings.toEscapedUnicode(), ma: - ignora i caratteri: '=', ':', '#',
	 * '!' - non esegue l'escape di un eventuale spazio come carattere iniziale
	 */
	public static String toEscapedUnicode(String unicodeString) {
		if ((unicodeString == null) || (unicodeString.length() == 0)) {
			return unicodeString;
		}
		int len = unicodeString.length();
		int bufLen = len * 2;
		StringBuilder outBuffer = new StringBuilder( bufLen );
		for (int x = 0; x < len; x++) {
			char aChar = unicodeString.charAt(x);
			// Handle common case first, selecting largest block that
			// avoids the specials below
			if ((aChar > 61) && (aChar < 127)) {
				if (aChar == '\\') {
					outBuffer.append('\\');
					outBuffer.append('\\');
					continue;
				}
				outBuffer.append(aChar);
				continue;
			}
			switch (aChar) {
				case ' ':
					outBuffer.append(' ');
					break;
				case '\t':
					outBuffer.append('\\');
					outBuffer.append('t');
					break;
				case '\n':
					outBuffer.append('\\');
					outBuffer.append('n');
					break;
				case '\r':
					outBuffer.append('\\');
					outBuffer.append('r');
					break;
				case '\f':
					outBuffer.append('\\');
					outBuffer.append('f');
					break;
				default:
					if ((aChar < 0x0020) || (aChar > 0x007e)) {
						outBuffer.append('\\');
						outBuffer.append('u');
						outBuffer.append(toHex((aChar >> 12) & 0xF));
						outBuffer.append(toHex((aChar >> 8) & 0xF));
						outBuffer.append(toHex((aChar >> 4) & 0xF));
						outBuffer.append(toHex(aChar & 0xF));
					} else {
						outBuffer.append(aChar);
					}
			}
		}
		return outBuffer.toString();
	}

	/**
	 * Convert a nibble to a hex character
	 *
	 * @param nibble
	 *            the nibble to convert.
	 * @return hex character
	 */
	private static char toHex(int nibble) {
		return hexDigit[(nibble & 0xF)];
	}



	public static boolean isNullEmpty( String string ){
		return string == null || string.isEmpty();
	}


	public static String nullSafeRightPad(String string, int size, String pad){
		if (string == null)
			return StringUtils.rightPad("", size, pad);
		else
			return StringUtils.rightPad(string, size, pad);
	}

	public static String nullSafeLeftPad(String string, int size, String pad){
		if (string == null)
			return StringUtils.leftPad("", size, pad);
		else
			return StringUtils.leftPad(string, size, pad);
	}
}
