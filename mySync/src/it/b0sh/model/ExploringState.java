package it.b0sh.model;

import java.nio.file.Path;
import java.util.List;
import java.util.Stack;

/**
 * Created by Simone on 30/01/2016.
 */
public class ExploringState {


    private Stack<Path> toBeExplored;
    private boolean exploringConcluded = false;
    private List<Path> foundList;
    private List<Path> foundTree;
    private List<Path> error;

    public List<Path> getError() {
        return error;
    }

    public void setError(List<Path> error) {
        this.error = error;
    }

    public List<Path> getFoundTree() {
        return foundTree;
    }

    public void setFoundTree(List<Path> foundTree) {
        this.foundTree = foundTree;
    }

    public List<Path> getFoundList() {
        return foundList;
    }

    public void setFoundList(List<Path> foundList) {
        this.foundList = foundList;
    }

    public Stack<Path> getToBeExplored() {
        return toBeExplored;
    }

    public void setToBeExplored(Stack<Path> toBeExplored) {
        this.toBeExplored = toBeExplored;
    }

    public boolean isExploringConcluded() {
        return exploringConcluded;
    }

    public void setExploringConcluded(boolean exploringConcluded) {
        this.exploringConcluded = exploringConcluded;
    }
}
