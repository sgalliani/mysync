package it.b0sh;

import it.b0sh.model.ExploringState;
import it.b0sh.model.SyncState;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

/**
 * Created by Simone on 17/01/2016.
 */
public class Syncronizer implements Runnable {

    private boolean verbose;
    private String source;
    private Path sourcePath;
    private String destination;
    private Path destionationPath;
    private List<Path> writeErrorList;
    private List<Path> readErrorList;
    private boolean fullSync;
    private SyncState syncState;


    private static boolean finished = false;

    private long fileCopied = 0;

    private int dirProcessed = 0;
    private int fileProcessed = 0;

    private boolean hasUnhandledErrors = true;

    private int currentStepProgress = 0;

    public Syncronizer(boolean verbose, String source, String destination, boolean fullSync) {
        this.verbose = verbose;
        this.source = source;
        this.destination = destination;
        this.fullSync = fullSync;
        this.syncState = SyncState.NOT_STARTED;
    }

    public void run(){

        sourcePath = Paths.get(source);
        destionationPath = Paths.get(destination);

        writeErrorList = new LinkedList<>();
        readErrorList = new LinkedList<>();

        try {
            ExploringState sourceExploring = new ExploringState();

            currentStepProgress = 0;

            syncState = SyncState.EXPLORING_SOURCE_PATH;

            explore(sourcePath,sourceExploring,verbose);

            readErrorList = sourceExploring.getError();

            System.out.println("Found " + sourceExploring.getFoundList().size() + " file in source path");

            if (sourceExploring.getError().size() > 0)
                System.out.println("Ecountered " + sourceExploring.getError().size() + " read errors");

            if(!destionationPath.toFile().exists())
                Files.createDirectory(destionationPath);

            currentStepProgress = 0;

            syncState = SyncState.COPING_FILE_AND_DIR;

            sync(sourceExploring);



            if (fullSync) {
                //Rileggo la destionazione per rimuovere ciò che non c'è nella sorgente

                ExploringState destinationExploring = new ExploringState();

                currentStepProgress = 0;

                syncState = SyncState.EXPLORING_DEST_PATH;

                explore(destionationPath,destinationExploring,verbose);

                readErrorList.addAll(destinationExploring.getError());

                currentStepProgress = 0;

                syncState = SyncState.REMOVING_FILE;

                removeIfNotPresent(destinationExploring);


            }

            hasUnhandledErrors = false;

            finished = true;
            syncState = SyncState.FINISHED;

        } catch (IOException e) {
            System.out.println("IO Error:" + Utils.exceptionToString(e));
            new RuntimeException("IO Error:" + e.getMessage(), e);
        }

    }


    private void sync(ExploringState sourceExploring) {

        Date date = new Date();

        sourceExploring.getFoundTree().stream().forEach(path1 -> {
            File destinationDir = new File(path1.toFile().getAbsolutePath().replace(source,destination));

            dirProcessed++;

            if (!destinationDir.exists()) {
                try {
                    Files.createDirectory(destinationDir.toPath());
                } catch (IOException e) {
                    writeErrorList.add(destinationDir.toPath());
                }
            }

            if (dirProcessed % 100 == 0 || dirProcessed == sourceExploring.getFoundTree().size() ) {
                System.out.println("Dir : " + Math.floor(dirProcessed * 100 / sourceExploring.getFoundTree().size()) + "%");

                currentStepProgress = Math.round(dirProcessed * 30 / sourceExploring.getFoundTree().size() );

            }



        });


        sourceExploring.getFoundList().stream().forEach(path -> {
            File destinationFile = new File(path.toFile().getAbsolutePath().replace(source,destination));

            fileProcessed++;

            if(!destinationFile.exists() || destinationFile.lastModified() < path.toFile().lastModified()) {
                if (verbose)
                    System.out.println("Copying " + path.toFile() + " to " + destinationFile);
                try {
                    Files.copy(path, destinationFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    fileCopied++;
                } catch (IOException e) {
                    writeErrorList.add(path);
                }

            }

            if (fileProcessed % 100 == 0 || fileProcessed == sourceExploring.getFoundList().size() ) {
                System.out.println("File : " + Math.floor(fileProcessed * 100 / sourceExploring.getFoundList().size()) + "%");

                currentStepProgress = Math.round( (fileProcessed * 70)  / sourceExploring.getFoundList().size()) + 30 ;
            }

        });

        System.out.println("Copied " + fileCopied + " files in : " + Double.toString(((new Date()).getTime() - date.getTime()) / 1000) + " seconds");

        if (writeErrorList.size() > 0)
            System.out.println("Ecountered " + writeErrorList.size() + " write errors");
    }


    private void recursiveDeleteDir(Path directory) throws IOException{

        Files.walkFileTree(directory, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                Files.delete(dir);
                return FileVisitResult.CONTINUE;
            }

        });


    }



    private void removeIfNotPresent(ExploringState destExploring){

        destExploring.getFoundList().stream().forEach(path -> {
            File sourceFile = new File(path.toFile().getAbsolutePath().replace(destination,source));

            if(!sourceFile.exists()) {
                try {
                    Files.delete(path);
                } catch (IOException e) {
                    writeErrorList.add(path);
                }

            }
        });

        destExploring.getFoundTree().stream().forEach(path1 -> {
            File sourceDir = new File(path1.toFile().getAbsolutePath().replace(destination,source));
            if (!sourceDir.exists()) {
                try {
                    recursiveDeleteDir(path1);
                } catch (IOException e) {
                    writeErrorList.add(path1);
                }
            }

        });

    }

    private static List<Path> explore(Path directory, ExploringState exploringState, boolean verbose) throws IOException {

        Date date = new Date();

        exploringState.setToBeExplored(new Stack<>());
        exploringState.setFoundList(new LinkedList<>());
        exploringState.setFoundTree(new LinkedList<>());
        exploringState.setError(new LinkedList<>());

        fileList(directory, exploringState, verbose);

        while(!exploringState.getToBeExplored().isEmpty()){
            Path dir = null;
            try {
                dir = exploringState.getToBeExplored().pop();
                fileList(dir,exploringState, verbose);
            } catch (IOException e) {
                System.err.println("Impossible to read " + dir + ". Error " + e.getMessage());
                exploringState.getError().add(dir);
            }
        }

        exploringState.setExploringConcluded(true);

        if (verbose)
            System.out.println(directory.toAbsolutePath() + " explored in : " + Double.toString(((new Date()).getTime() - date.getTime()) / 1000) + " seconds");

        return exploringState.getFoundList();

    }


    private static void fileList(Path directory, ExploringState exploringState, boolean verbose) throws IOException {

        DirectoryStream<Path> directoryStream = Files.newDirectoryStream(directory);

        for (Path path : directoryStream) {

            if (path.toFile().isDirectory()) {
                if (verbose)
                    System.out.println("Found " + exploringState.getFoundList().size() + " files. Exploring : " + path.toAbsolutePath() );
                exploringState.getFoundTree().add(path);
                exploringState.getToBeExplored().push(path);
            } else {
                exploringState.getFoundList().add(path);
            }
        }

    }

    public int getCurrentStepProgress() {
        return currentStepProgress;
    }

    public SyncState getSyncState() {
        return syncState;
    }

    public List<Path> getWriteErrorList() {
        return writeErrorList;
    }

    public void setWriteErrorList(List<Path> writeErrorList) {
        this.writeErrorList = writeErrorList;
    }

    public List<Path> getReadErrorList() {
        return readErrorList;
    }

    public void setReadErrorList(List<Path> readErrorList) {
        this.readErrorList = readErrorList;
    }

    public boolean isHasUnhandledErrors() {
        return hasUnhandledErrors;
    }

    public void setHasUnhandledErrors(boolean hasUnhandledErrors) {
        this.hasUnhandledErrors = hasUnhandledErrors;
    }

    public static boolean isFinished() {
        return finished;
    }
}
