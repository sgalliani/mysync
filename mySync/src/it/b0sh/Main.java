package it.b0sh;

import org.apache.commons.cli.*;

public class Main {

    private static boolean verbose;
    private static boolean fullSync;
    private static String source;
    private static String destination;
    private static Syncronizer batch;
    private static Throwable uncaughtException;


    public static void main(String[] args) {


        Thread.currentThread().setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                uncaughtException = e;
            }
        });



        parseArgs(args);

        batch = new Syncronizer(verbose,source,destination,fullSync);

        batch.run();

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                if (batch.isHasUnhandledErrors()) {
                    String ex = "";
                    if (uncaughtException != null) {
                        ex += Utils.exceptionToString(uncaughtException);
                    }
                    System.out.println("Errori imprevisti non hanno permesso il regolare svolgimento del processo " + ex);
                }
            }
        });

    }

    private static void parseArgs(String[] args){
        Options options = new Options();

        Option verboseOption = new Option("v",false,"verbose mode");
        Option sourcePath = new Option("s",true,"source path");
        Option destinationPath = new Option("d",true,"destination path");
        Option help = new Option("h",false,"this Help");
        Option fullSyncOption = new Option("f",false,"Full Sync mode");
        options.addOption(verboseOption);
        options.addOption(help);
        options.addOption(sourcePath);
        options.addOption(destinationPath);
        options.addOption(fullSyncOption);
        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(options,args);
            if(cmd.hasOption("h")){
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("java -jar mysync.jar",options);
                batch.setHasUnhandledErrors(false);
                System.exit(0);
            }

            if(cmd.hasOption("s")){
                source = cmd.getOptionValue("s");

            } else {
                System.exit(0);
            }

            if(cmd.hasOption("d")){
                destination = cmd.getOptionValue("d");

            } else {
                System.exit(0);
            }

            if(cmd.hasOption("v")){
                verbose = true;
            }

            if(cmd.hasOption("f")){
                fullSync = true;
            }

        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
