package it.b0sh.gui;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Simone on 31/01/2016.
 */
public class MainViewController implements Initializable {
    @FXML
    public Label qualeFaseLabel;
    @FXML
    public Label readErrorLabel;
    @FXML
    public ProgressBar progressBar;
    @FXML
    public Label writeErrorLabel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
